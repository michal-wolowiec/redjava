package rest.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.UserRepository;
import dao.MockRepositoryCatalog;
import dao.RepositoryCatalog;
import domain.User;
import rest.dto.UserDto;
import rest.dto.SearchDto;;

@Path("users")
public class UserService {
	
	RepositoryCatalog db = new MockRepositoryCatalog();
	
	
	
	@PUT
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response add(UserDto dto){
		
		User u = new User();
		
		u.setUsername(dto.getUsername());
		u.setPassword(dto.getPassword());
		u.setSex(dto.getSex());
		u.setAge(dto.getAge());
		u.setWeight(dto.getWeight());
		u.setHeight(dto.getHeight());
		u.setEye(dto.getEye());
		u.setHair(dto.getHair());
		u.setCity(dto.getCity());
		u.setVoiv(dto.getVoiv());
		u.setEducation(dto.getEducation());
		u.setAbout(dto.getAbout());
		u.setWho(dto.getWho());
		
		db.users().add(u);
		
		return Response.status(201).build();
	}
	
	@DELETE
	@Path("/del")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response delete(UserDto dto){
		
		db.users().delete(db.users().get(dto.getId()));
		
		return Response.status(201).build();
	}
	
	@POST
	@Path("/edit")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response edit(UserDto dto){
		
		db.users().get(dto.getId()).setUsername(dto.getUsername());
		db.users().get(dto.getId()).setPassword(dto.getPassword());
		db.users().get(dto.getId()).setSex(dto.getSex());
		db.users().get(dto.getId()).setAge(dto.getAge());
		db.users().get(dto.getId()).setWeight(dto.getWeight());
		db.users().get(dto.getId()).setHeight(dto.getHeight());
		db.users().get(dto.getId()).setEye(dto.getEye());
		db.users().get(dto.getId()).setHair(dto.getHair());
		db.users().get(dto.getId()).setCity(dto.getCity());
		db.users().get(dto.getId()).setVoiv(dto.getVoiv());
		db.users().get(dto.getId()).setEducation(dto.getEducation());
		db.users().get(dto.getId()).setAbout(dto.getAbout());
		db.users().get(dto.getId()).setWho(dto.getWho());
		return Response.status(201).build();
	}
	
	@PUT
	@Path("/search")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<UserDto> searchResults(SearchDto dto){
		List<UserDto> results = new ArrayList<UserDto>();
		
		for(User u : db.users().getAll()){
			UserDto udto = new UserDto();
			udto.setAge(u.getAge());
			if(udto.getAge() > dto.getFage() && udto.getAge() < dto.getLage()){
				results.add(udto);
			}
		}
		
		return results;
	}
	
	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<UserDto> getAll(){
		List<UserDto> result = new ArrayList<UserDto>();
		
		for(User u: db.users().getAll()){
			UserDto dto = new UserDto();
			dto.setId(u.getId());
			dto.setUsername(u.getUsername());
			dto.setPassword(u.getPassword());
			dto.setAge(u.getAge());
			result.add(dto);
		}
		
		return result;
	}
}
