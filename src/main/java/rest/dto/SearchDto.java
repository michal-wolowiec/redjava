package rest.dto;

public class SearchDto {
	
	private int fage;
	private int lage;
	private int fheight;
	private int lheight;
	private int fweight;
	private int lweight;
	private String seducation;
	private String pname;
	private String scity;
	private String svoiv;
	
	public int getFage() {
		return fage;
	}
	public void setFage(int fage) {
		this.fage = fage;
	}
	public int getLage() {
		return lage;
	}
	public void setLage(int lage) {
		this.lage = lage;
	}
	public int getFheight() {
		return fheight;
	}
	public void setFheight(int fheight) {
		this.fheight = fheight;
	}
	public int getLheight() {
		return lheight;
	}
	public void setLheight(int lheight) {
		this.lheight = lheight;
	}
	public int getFweight() {
		return fweight;
	}
	public void setFweight(int fweight) {
		this.fweight = fweight;
	}
	public int getLweight() {
		return lweight;
	}
	public void setLweight(int lweight) {
		this.lweight = lweight;
	}
	public String getSeducation() {
		return seducation;
	}
	public void setSeducation(String seducation) {
		this.seducation = seducation;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public String getScity() {
		return scity;
	}
	public void setScity(String scity) {
		this.scity = scity;
	}
	public String getSvoiv() {
		return svoiv;
	}
	public void setSvoiv(String svoiv) {
		this.svoiv = svoiv;
	}
	
}
