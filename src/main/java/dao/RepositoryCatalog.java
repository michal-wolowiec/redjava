package dao;

import domain.User;

public interface RepositoryCatalog {

	public Repository<User> users();
}
