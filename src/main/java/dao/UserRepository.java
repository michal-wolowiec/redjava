package dao;

import java.util.ArrayList;
import java.util.List;

import domain.User;

public class UserRepository implements Repository<User>{

	
	private static List<User> users = new ArrayList<User>();
	
	
	@Override
	public synchronized void add(User entity) {
		int currentId=0;
		for(User u : users){
			currentId++;
		}
		for(User u : users){
			if(u.getId()==currentId+1){
				currentId++;
			}
		}
		entity.setId(currentId+1);
		users.add(entity);
	}

	@Override
	public void delete(User entity) {
		users.remove(entity);
	}

	@Override
	public User get(int id) {
		for(User u : users){
			if(u.getId()==id) return u;
		}
		return null;
	}

	@Override
	public List<User> getAll() {
		return users;
	}

}
