package dao;

import domain.User;

public class MockRepositoryCatalog implements RepositoryCatalog{

	@Override
	public Repository<User> users() {
		return new UserRepository();
	}

}
