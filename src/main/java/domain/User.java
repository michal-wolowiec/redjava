package domain;

public class User {

	private int id;
	
	private String username;
	private String password;
	private String sex;
	private int age;
	private int weight;
	private int height;
	private String eye;
	private String hair;
	private String city;
	private String voiv;
	private String education;
	private String about;
	private String who;
	
	public User(String username, String password, String sex, int age, int weight, int height, String eye, String hair, String city, String education, String about, String who){
		this.username = username;
		this.password = password;
		this.sex = sex;
		this.age = age;
		this.weight = weight;
		this.height = height;
		this.eye = eye;
		this.hair = hair;
		this.city = city;
		this.education = education;
		this.about = about;
		this.who = who;
	}

	public User() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getEye() {
		return eye;
	}

	public void setEye(String eye) {
		this.eye = eye;
	}

	public String getHair() {
		return hair;
	}

	public void setHair(String hair) {
		this.hair = hair;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getWho() {
		return who;
	}

	public void setWho(String who) {
		this.who = who;
	}

	public String getVoiv() {
		return voiv;
	}

	public void setVoiv(String voiv) {
		this.voiv = voiv;
	}
}
