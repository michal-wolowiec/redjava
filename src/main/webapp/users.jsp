<%@ page language="java" contentType="text/html"
    pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>    
<t:layout>
	<jsp:attribute name="styles">
		<link rel="stylesheet" href="styles.css">
	</jsp:attribute>
	<jsp:attribute name="scripts">
		<script type="text/javascript" src="scripts/user/UserViewModel.js"></script>
		<script type="text/javascript" src="scripts/user/UserListViewModel.js"></script>
		<script type="text/javascript">
		$(function(){
			$.ajax({
	            url: "http://localhost:8080/servletjspdemo/rest/users/all",
	            type: "GET",
	            contentType: "application/json",
	            success: function (data) {
	                var viewModel = new UserListViewModel(data);
	                ko.applyBindings(viewModel);
	            },
	            error: function (XMLHttpRequest, testStatus, errorThrown) {
	               alert("nie udało się")

	            }
	        });
		});
		</script>
	
	</jsp:attribute>
	<jsp:body>
		All users:<br/>
		<ul data-bind="foreach: users">
			<li>ID: <span data-bind="text:id"></span></li>
			<li>Login: <span data-bind="text:username"></span></li>
			<li>Password: <span data-bind="text:password"></span></li>
			<li>Age: <span data-bind="text:age"></span></li><br/>
		</ul>
	</jsp:body>
	
</t:layout>