function UserViewModel(model){
	var listOfUsers='listOfUsers';
	ko.mapping.fromJS(model,{},this);
	var self = this;

	self.show = function(){
		alert(ko.mapping.toJSON(self));
	}
	
	self.add = function(){
		$.ajax({
            url: "http://localhost:8080/servletjspdemo/rest/users/add",
            type: "PUT",
            data: ko.mapping.toJSON(self),
            contentType: "application/json",
            success: function (data) {
                alert("udało się");
            },
            error: function (XMLHttpRequest, testStatus, errorThrown) {
               alert("nie udało się");

            }
        });
	}
	
	self.edit = function(){
		$.ajax({
            url: "http://localhost:8080/servletjspdemo/rest/users/edit",
            type: "POST",
            data: ko.mapping.toJSON(self),
            contentType: "application/json",
            success: function (data) {
                alert("User zostal zedytowany");
            },
            error: function (XMLHttpRequest, testStatus, errorThrown) {
               alert("nie udało się");

            }
        });
	}
	
	self.del = function(){
		$.ajax({
            url: "http://localhost:8080/servletjspdemo/rest/users/del",
            type: "DELETE",
            data: ko.mapping.toJSON(self),
            contentType: "application/json",
            success: function (data) {
                alert("User zostal usuniety");
            },
            error: function (XMLHttpRequest, testStatus, errorThrown) {
               alert("nie udało się");

            }
        });
	}
	
	self.search = function(){
		$.ajax({
            url: "http://localhost:8080/servletjspdemo/rest/users/search",
            type: "PUT",
            data: ko.mapping.toJSON(self),
            contentType: "application/json",
            success: function (data) {
                alert("Oto Wyniki wyszukiwania.");
            },
            error: function (XMLHttpRequest, testStatus, errorThrown) {
               alert("cos poszlo nie tak");

            }
        });
	}
	
}