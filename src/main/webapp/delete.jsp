<%@ page language="java" contentType="text/html"
    pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates/profile" %>    
<t:layout>
	<jsp:attribute name="styles">
		<link rel="stylesheet" href="styles.css">
	</jsp:attribute>
	<jsp:attribute name="scripts">
		<script type="text/javascript" src="scripts/user/UserViewModel.js"></script>	
		<script type="text/javascript">
			$(function(){
				var model = {
						id:0,
				};
				var viewModel = new UserViewModel(model);
				var data = ko.toJSON( {
	            	messege:'data from web'
	            });
				ko.applyBindings(viewModel);
				
			})
		</script>
	</jsp:attribute>
	<jsp:body>
		Delete User:<br/>
		<label>Which user do you want to delete?(Input ID): <input type="text" data-bind="value: id"/></label><br/><br/>
		<button data-bind="click:show">Show</button><br/><br/>
		<button data-bind="click:del">Delete</button><br/><br/>
	</jsp:body>
	
</t:layout>