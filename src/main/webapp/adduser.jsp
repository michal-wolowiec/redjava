<%@ page language="java" contentType="text/html"
    pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>    
<t:layout>
	<jsp:attribute name="styles">
		<link rel="stylesheet" href="styles.css">
	</jsp:attribute>
	<jsp:attribute name="scripts">
		<script type="text/javascript" src="scripts/user/UserViewModel.js"></script>	
		<script type="text/javascript">
			$(function(){
				var model = {
						username:'',
						password:'',
						sex:'',
						age:0,
						weight:0,
						height:0,
						eye:'',
						hair:'',
						city:'',
						voiv:'',
						education:'',
						about:'',
						who:'',
				};
				var viewModel = new UserViewModel(model);
				var data = ko.toJSON( {
	            	messege:'data from web'
	            });
				ko.applyBindings(viewModel);
				
			})
		</script>
	</jsp:attribute>
	<jsp:body>
		Add User:<br/>
		<label>UserName <input type="text" data-bind="value: username"/></label><br/><br/>
		<label>Password <input type="password" data-bind="value: password"/></label><br/><br/>
		<label>Sex <input type="text" data-bind="value: sex"/></label><br/><br/>
		<label>Age <input type="text" data-bind="value: age"/></label><br/><br/>
		<label>weight <input type="text" data-bind="value: weight"/></label><br/><br/>
		<label>height <input type="text" data-bind="value: height"/></label><br/><br/>
		<label>eye <input type="text" data-bind="value: eye"/></label><br/><br/>
		<label>hair <input type="text" data-bind="value: hair"/></label><br/><br/>
		<label>city <input type="text" data-bind="value: city"/></label><br/><br/>
		<label>Voivodeship:<select data-bind="value: voiv">
				<option>Dolnoslaskie</option>  
				<option>Kujawsko-Pomorskie</option> 
				<option>Lubeskie</option>
				<option>Lubuskie</option>
				<option>Lodzkie</option>
				<option>Malopolskie</option>
				<option>Mazowieckie</option>
				<option>Opolskie</option>
				<option>Podkarpackie</option>
				<option>Podlaskie</option>
				<option>Pomorskie</option>
				<option>Slaskie</option>
				<option>Swietokrzyskie</option>
				<option>Warminsko-Mazurskie</option>
				<option>Wielkopolskie</option>
				<option>Zachodniopomorskie</option>
				</select></label><br /> <br />
		<label>education <input type="text" data-bind="value: education"/></label><br/><br/>
		<label>about<br/><textarea rows="5" cols="40" data-bind="value: about"></textarea></label><br/><br/>
		<label>who<br/><textarea rows="5" cols="40" data-bind="value: who"></textarea></label><br/><br/>
		<button data-bind="click:show">Show</button><br/><br/>
		<button data-bind="click:add">Add</button><br/><br/>
	</jsp:body>
	
</t:layout>