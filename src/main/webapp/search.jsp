<%@ page language="java" contentType="text/html"
    pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates/profile" %>    
<t:layout>
	<jsp:attribute name="styles">
		<link rel="stylesheet" href="styles.css">
	</jsp:attribute>
	<jsp:attribute name="scripts">
		<script type="text/javascript" src="scripts/user/UserViewModel.js"></script>	
		<script type="text/javascript">
			$(function(){
				var model = {
						fage:0,
						lage:0,
						fheight:0,
						lheight:0,
						fweight:0,
						lweight:0,
						seducation:'',
						pname:'',
						scity:'',
						svoiv:'',
				};
				var viewModel = new UserViewModel(model);
				var data = ko.toJSON( {
	            	messege:'data from web'
	            });
				ko.applyBindings(viewModel);
				
			})
		</script>
	</jsp:attribute>
	<jsp:body>
		Search:<br/>
		<label>Age: <input type="text" data-bind="value: fage" maxlength="2" size="2"/> - <input type="text" data-bind="value: lage" maxlength="2" size="2"/></label><br/><br/>
		<label>Height: <input type="text" data-bind="value: fheight" maxlength="2" size="2"> - <input type="text" data-bind="value: lheight" maxlength="2" size="2"></label><br/><br/>
		<label>Weight: <input type="text" data-bind="value: fweight" maxlength="2" size="2"> - <input type="text" data-bind="value: lweight" maxlength="2" size="2"></label><br/><br/>
		<label>Education: <select data-bind="value: seducation">
		<option>Basic</option>
		<option>Secondary</option>
		<option>Some University</option>
		<option>Higher</option>
		</select></label><br/><br/>
		<label>Profile Name: <input type="text" data-bind="value: pname" /></label><br/><br/>
		<label>City <input type="text" data-bind="value: scity" /></label><br/><br/>
		<label>Voivodeship:<select data-bind="value: svoiv">
				<option>Dolnoslaskie</option>  
				<option>Kujawsko-Pomorskie</option> 
				<option>Lubeskie</option>
				<option>Lubuskie</option>
				<option>Lodzkie</option>
				<option>Malopolskie</option>
				<option>Mazowieckie</option>
				<option>Opolskie</option>
				<option>Podkarpackie</option>
				<option>Podlaskie</option>
				<option>Pomorskie</option>
				<option>Slaskie</option>
				<option>Swietokrzyskie</option>
				<option>Warminsko-Mazurskie</option>
				<option>Wielkopolskie</option>
				<option>Zachodniopomorskie</option>
				</select></label><br /> <br />
		<button data-bind="click:show">Show</button><br/><br/>
		<button data-bind="click:search">Search</button><br/><br/>
	</jsp:body>
	
</t:layout>